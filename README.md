# Welcome to the Qipp Testen ATDD Workshop

Author: Jeroen Ansink

## Dependencies

Node.js is installed

After you cloned this project, in your terminal go to the project folder and type:

<b>npm install</b>

## Page object pattern
<ul>
<li>Feature files in e2e/features folder</li>
<li>Page object files in e2e/page_objects folder</li>
<li>Step definition files in e2e/step_definitions folder</li>
</ul>

## How to run the e2e tests?
In your terminal go to the project folder and type:

<b>npm run test:e2e</b>

## Where to find the test report

In the reports folder there is an index.html file. Open it in your browser.

## How to run in browser instead of headless?

In the file <i>protractor.conf.js</i> remove the argument --headless from chromeOptions.
