//Jeroen Ansink, QIPP Testen BV 2018

import {$, $$, by, element, ElementFinder, ElementArrayFinder, browser} from 'protractor';

export class Homepage {
    qippLogo: ElementFinder;

    constructor() {
        this.qippLogo = $('#comp-jgezvtycimgimage');
    }
}
