//Jeroen Ansink, QIPP Testen BV 2018

exports.config = {
    directConnect: true,
    SELENIUM_PROMISE_MANAGER: false,
    baseUrl: 'http://www.qipp-testen.nl/',
    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: ['--headless', '--disable-gpu', '--disable-popup-blocking', '--window-size=1360x768', 'no-sandbox']
        }
    },
    onPrepare: () => {

        browser.waitForAngularEnabled(false);

    },
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: ['features/*.feature'],
    cucumberOpts: {
        'require-module': 'ts-node/register',
        strict: true,
        format: 'json:reports/e2e/protractor-report.json',
        require: ['step_definitions/*.steps.ts'],
        tags: ['~@todo']
      },
    plugins: [{
        package: "protractor-multiple-cucumber-html-reporter-plugin",
        options: {
            automaticallyGenerateReport: true,
            removeExistingJsonReportFile: true,
            removeOriginalJsonReportFile: false,
            reportPath: "./reports/e2e/",
            reportName: "Qipp Testen - ATDD Workshop"
        }
    }]
};
