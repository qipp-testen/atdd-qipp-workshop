//Jeroen Ansink, QIPP Testen BV 2018

import {Given, When, Then, setDefaultTimeout} from 'cucumber';
import {element, by, browser, ExpectedConditions, protractor} from 'protractor';
import {Homepage} from '../page_objects/QippTestenHomepage.po';
const chai = require('chai');
const chaiString = require('chai-string');
const chaiSmoothie = require('chai-smoothie');

chai.use(chaiSmoothie)
    .use(chaiString);
const expect = chai.expect;
const homepage = new Homepage();

setDefaultTimeout(60 * 1000);

Given(/^The Qipp Testen website is loaded$/, async () => {
  await browser.get('');
});

Then(/^the Qipp Testen logo is visible$/, async () => {
  await browser.switchTo().defaultContent();
  await browser.sleep(2000);
  await expect(homepage.qippLogo).to.be.displayed;
});
